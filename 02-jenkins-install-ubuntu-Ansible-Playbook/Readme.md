# Installing Jenkins on Ubuntu 18 Using An Ansible Playbook
---
## Steps:
   - Use **Vagrant** to launch two Linux Ubuntu/Bionic64 VMs (See the *Vagranfile*). 
     * The first VM acts as the Ansible Control Node. 
     * The second VM acts as the target Node on which Jenkins will be installed.
   - Write the **Ansible** Playbook. An example it provided in *jenkins-install-unbuntu.yml*
        
     ```yaml
       ---
       - name: Ansible Install Jenkins on Ubuntu18 Server
         gather_facts: false
         hosts: jenkins
         become: true
         tasks:
            - name: APT Update
              command: apt update -y
            
            - name: Install Java
              command: apt install -y openjdk-8-jdk

            - name: Install key for the Jenkins apt repository
              apt_key:
                url: "https://pkg.jenkins.io/debian/jenkins-ci.org.key"
                state: present

            - name: Install access to the Jenkins apt repository
              apt_repository:
              repo: deb http://pkg.jenkins.io/debian-stable binary/
              state: present
              filename: 'jenkins-stable'

            - name: APT Update for the Jenkins repository
               command: apt update -y

            - name: Install Jenkins package
              apt:
              name: jenkins
              state: present

            - name: Wait for Jenkins to start up
              wait_for: timeout=35

            - name: Get Jenkins password
              shell: cat /var/lib/jenkins/secrets/initialAdminPassword
              changed_when: false
              register: result

            - name: Password to "Unlock Jenkins"
              debug:
              var: result.stdout
     ```
 - Configure SSH in order to enable Ansible to connect to the target VM.
      ``` sh
        # On the Control node, create an SSH keypair.
        $ ssh-keygen -t rsa
        
        # On the Control node, copy the public key to the target machine.
        $ ssh-copy-id  vagrant@10.0.15.11
        
        # If the previous command does not work, work around it as follows:
            1- On the target machine, edit sshd_config file 
               $ sudo nano /etc/ssh/sshd_config
               and Change this line:
               PasswordAuthentication no
               to
               PasswordAuthentication yes
            2- Restart the sshd daemon:
               $ sudo systemctl restart sshd

 - Add the inventory (the hosts file in this example) 
      ``` sh 
          [jenkins]
          10.0.15.11  ansible_python_interpreter=/usr/bin/python3
      ```
      * Note: ansible_python_interpreter instructs Ansible to use the python3 interperpter which is the default python version installed in Ubuntu/Bionic64.
      
 - Run the Ansible playbook
      ``` sh
         $ ansible-playbook -i hosts jenkins-install-unbuntu.yml
      ```
     