# JenkinsCI : Continious Deployment
---
## What is Continuous Deployment ?
The process of continuously deploying production-ready features into the production environment, or to the end user, is termed as Continuous Deployment.
Continuous Deployment in a holistic sense means, *the process of making production-ready features go live instantly without any intervention*. This includes building features in an agile manner, integrating and testing them continuously, and deploying them into the production environment without any breaks.


## How Continuous Deployment is different from Continuous Delivery
First, the features are developed, and then they go through a cycle, or Continuous Integration, or through testing of all kinds. Anything that passes the various tests is considered as a production-ready feature. These production-ready features are then labeled in Artifactory (not shown in this book) or kept separately to segregate them from nonproduction ready features.
This is similar to the manufacturing production line. The raw product goes through phases of modifications and testing. Finally, the finished product is packaged and stored in the warehouses. From the warehouses, depending on the orders, it gets shipped to various places. The product doesn't get shipped immediately after it's packaged.


## Creating a production Server
We will extend our Continuous Delivery pipeline to automatically deploy
fully testing binary artifacts on our production server. This server is provisioned using Vagrant.


The figure belows show the stages of the pipeline.
![Pipeline](images/pipeline.jpg)

  
   
## Implementation details

See **Chapter 9 : Continuous Deployment Using Jenkins** of *Nikhil Pathania* Book **Continuous Integration Using Jenkins, 2nd Edition**

![book](images/book.jpg)

