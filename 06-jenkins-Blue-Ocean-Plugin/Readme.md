# Jenkins Pipelines; A new user experience with Blue Ocean
---
Blue Ocean is a project that rethinks the user experience of Jenkins, modelling and presenting the process of software delivery by surfacing information that's important to development teams with as few clicks as possible, while still staying true to the extensibility that is core to Jenkins [See more details here](https://jenkins.io/blog/2016/05/26/introducing-blue-ocean/).

## Installing and Getting Started with Blue Ocean
To install the Blue Ocean suite of plugins to your Jenkins instance:

- If required, ensure you are logged in to Jenkins (as a user with the Administer permission).
- From the Jenkins home page (i.e. the Dashboard of the Jenkins classic UI), click Manage Jenkins on the left and then Manage Plugins in the center.
- Click the Available tab and type blue ocean into the Filter text box, which filters the list of plugins to those whose name and/or description contains the words "blue" and "ocean".
  
    [See more details here](https://jenkins.io/doc/book/blueocean/getting-started/).
## Creating a Pipeline with Blue Ocean
To start setting up your Pipeline project in Blue Ocean, at the top-right of the Blue Ocean Dashboard, click the **New Pipeline** button.
If your Jenkins instance is new or has no Pipeline projects or other items configured (and the Dashboard is empty), Blue Ocean displays a Welcome to Jenkins message box on which you can click the **Create a new Pipeline** button to start setting up your Pipeline project.
[See more details here](https://jenkins.io/doc/book/blueocean/creating-pipelines/).